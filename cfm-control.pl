#!/usr/bin/perl
# cfm-control.pl
use 5.014;

## Because old habits die hard and sometimes prove useful.
my $Exists = ''; my $tatus = ''; my $Rezult = ''; my $witch = ''; my $Command2run = ''; $Rezult = ''; my $tackStatus = '';
my $Action = $ARGV[0]; my $EnvName = $ARGV[2]; my $tackPath = $ARGV[3];  my $tackName = "$ARGV[1]";

## Todo :: Update the script to allow for non-stage naming to work with the below conditional.
#if (!defined($ARGV[2])) { 
#        $tackName = $ARGV[1]; 
#        }
#        else { 
#        $tackName = "$ARGV[1]-$EnvName"; 
#        } 


## Main ##
given($Action) {
    ## Create or Update the stack. 
    when (/create|update/) {
        $tackStatus = &GetStackStatus($tackName);
        if ( $tackStatus == 5 ) { print "\n$tackName had been inititiated but failed to complete it's launch\n"; exit 1; }
        elsif ( $tackStatus == 1 ) { print "\n\nStack \'$tackName\' exists. Updating\n";
            ## stack-update
            print "\n\nUpdating stack \'$tackName\'\n";
            my $Action=$ARGV[0]; my $tackName=$ARGV[1]; my $EnvName = $ARGV[2]; my $tackPath = $ARGV[3];
            &JustDoIt("aws cloudformation update-stack --stack-name $tackName --template-body file://$tackPath/$tackName.json");
        } elsif ( $tackStatus == 0 ) {
            print "\n\nCreating stack \'$tackName\'\n";
            my $Action=$ARGV[0]; my $tackName=$ARGV[1]; my $EnvName = $ARGV[2]; my $tackPath = $ARGV[3];
            &JustDoIt("aws cloudformation create-stack --stack-name $tackName --template-body file://$tackPath/$tackName.json");
        }
        exit 0;
    }

    ## delete-stack
    when (/delete/) {
        $tackStatus = &GetStackStatus($tackName);
        if ( $tackStatus == 1 || $tackStatus == 5 ) { print "\n\nDeleting Stack \'$tackName\'.\n"; 
            print "\n\nIf the Delete fails you will need to consult the AWS UI to find and fix the cause of the issue\n\n";
            &JustDoIt("aws cloudformation delete-stack --stack-name $tackName");
            exit 0;
        } elsif ( $tackStatus == 0 ) { print "\n\nStack Does not exist. Nothing to Delete, no action taken.\'\n"; exit 1; }
    }

    ## Get The Status
    when (/status/) {
        $tackStatus = &GetStackStatus($tackName);
        if ( $tackStatus == 1 ) { print "\nStack found and appears to be healthy\n"; } 
        elsif ( $tackStatus == 5 ) { print "\n$tackName had been inititiated but failed to complete it's launch\n"; }
        else { print "\n$tackName not found\n"}
    }

    ## Validate the Json - This is ugly but it's better this way.
    when (/validate/) {
        my $ExitStatus=1;
        my $Rezult = &JustDoIt("aws cloudformation validate-template --template-body file://$tackPath/$tackName.json");
        $ExitStatus=0 ; exit $ExitStatus;
        # If this fails it will throw an exception with useful data. If it passes it doesn't provide anything useful. Just going to let it stand.
    }
default { &JustDoIt("cat usage.txt"); print "$Rezult"; exit 1; }
}

## Get Stack Status
sub GetStackStatus($) {
    my $tackName = shift;
    $Exists = &DoesItLive($tackName);
    if ( $Exists == 1 ) {
        my $Rezult = &JustDoIt("aws cloudformation describe-stacks --stack-name $tackName | grep \'StackStatus\' | gawk \'{ print \$2}\' | tr -d \'\"\' | tr -d \',\'");
        my $witch = &trim($Rezult);
        given ($witch) {
            when (/CREATE_IN_PROGRESS/) { print "\nCreation in Progress.\n"; $tackStatus = 3; }
            when (/DELETE_IN_PROGRESS/) { print "\nDelete in Progress.\n"; $tackStatus = 4; }
            when (/CREATE_COMPLETE/)    { $tackStatus = 1; }
            when (/ROLLBACK_COMPLETE/)  { $tackStatus = 5; }
            ## Return Failure status for these
            when ([/CREATE_FAILED|ROLLBACK_FAILED|UPDATE_ROLLBACK_FAILED/]) { print "\nSomething is not right here.\n";
                die "One of the following conditions is true:\n ROLLBACK_COMPLETE, CREATE_FAILED, ROLLBACK_FAILED, UPDATE_ROLLBACK_FAILED\n";
            }
            when (/DELETE_FAILED/){
                print "\nPlease refer to the AWS UI for more info\n";
                die "The Delete has failed";
            }
            default { print "Nothing to do or what you were trying did not work."; }
        }
    return $tackStatus;
    } elsif ( $Exists == 0 ) { $tatus = 0; return $tackStatus; }

}

## Does it Live?
sub DoesItLive($) {
  ## Make sure a stack does, or does not exist. AWS throws an exception when a stack-name doesn't exist.
  print "\n\nQuerying AWS, This could take several minutes, Please wait.\n";
  $witch = &JustDoIt("aws cloudformation describe-stacks | grep /$tackName/ ");
  if ($witch eq '') { $Exists=0; } else { $Exists=1; }
return $Exists;
}

## Clear off whitespace.
sub trim($) { $witch = shift; $witch =~ s/^\s+|\s+$//g; return $witch }

## JustDoIt.
sub JustDoIt($) { my $Command2Run = shift; $Rezult = `$Command2Run`; return $Rezult; }
