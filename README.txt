Reason for this project: 
~~~
Troposphere is a collection of Python scripts designed to create static json 
files for cloudformations and has no logic to call AWS to initiate action. This 
controller was built to add logic to a CI/CD pipeline for action based on the 
current condition of the stack. 

What we have now: 
~~~
Simple bash commands used in CI/CD solutions lack the intelligence to determine 
if a stack exists and needs to be updated, or doesn't exist and needs to be created
This utility provides that abilty and a little more. 

What's needed is something with logic to determine what the current state of the
stack is before calling AWS, then call the correct function. Create, Update, 
Delete, Get Status, Etc. 


Workflow with cfm-controller:
- Troposphere script is run to create the json for the cloudformation. 
- Controller Checks to see if a stack exists. Creates the stack if it doesn't,
  Updates stack if it does. 
  - Can also be called to check the status of and delete stacks. 
  - Can be used for any stack regardless of how the stack was formed. 

  Todo: 
  - Expand the status check to determine if a Stack is eligible to update. 
  - Add production flag. (Needed for production stacks.)
  - Convert this from Perl to Python.
