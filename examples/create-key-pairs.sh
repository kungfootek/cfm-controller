#!/bin/bash

#profile=prod
pairs="dexcom"

for pair in $pairs; do
    cmd="aws ec2 create-key-pair --key-name $pair"
    if [ ! -z "$profile" ]; then
	cmd="$cmd --profile $profile"
    fi
    echo $pair
    result=`eval $cmd`
    if [ $? -eq 0 ]; then
	key=`echo "$result" | jq -r .KeyMaterial`
	echo "$key" > $pair.key
        chmod 0600 $pair.key
	echo "Private key saved as $pair.key"
    else
	echo "Failed"
    fi
done
