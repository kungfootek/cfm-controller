#!/usr/bin/python

from troposphere import GetAtt, Join, Output, Ref
from troposphere import Template
from troposphere.rds import DBCluster, DBSubnetGroup, DBInstance, DBClusterParameterGroup
from troposphere.route53 import RecordSetType
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('environment')
args = parser.parse_args()

database_name = 'kft_drupal_7'
instance_size = 'db.r3.large'
hostname = ''
password = os.getenv('AURORA_ADMIN_PASSWORD', 'ChangeMe') 

template = Template()
template.add_version("2010-09-09")
template.add_description("")

subnet_group = template.add_resource(
  DBSubnetGroup('AuroraSubnetGroup',
    DBSubnetGroupDescription = "Aurora",
    SubnetIds = ["subnet-#######","subnet-########","subnet-#######"]
  )
)

parameter_group_name = 'DSAuroraClusterUTF8' + args.environment
parameter_group = template.add_resource(
  DBClusterParameterGroup(
    parameter_group_name,
    Description = 'Aurora 5.6 with UTF-8 CharSet',
    Family = 'aurora5.6',
    Parameters = {
      "character_set_database": "utf8"
    }
  )
)

cluster = template.add_resource(
  DBCluster('AuroraCluster',
    Engine = 'aurora',
    DatabaseName = database_name + args.environment,
    DBSubnetGroupName = Ref("AuroraSubnetGroup"),
    DBClusterParameterGroupName = Ref(parameter_group_name),
    MasterUsername = 'admin',
    MasterUserPassword = password,
    VpcSecurityGroupIds = ["sg-######"] 
  )
)

instance = template.add_resource(
  DBInstance('AuroraInstance',
    DBInstanceIdentifier = database_name + args.environment,
    DBClusterIdentifier = Ref("AuroraCluster"),
    DBInstanceClass = instance_size,
    Engine = 'aurora',
    Tags = [
      {
        "Key": "key1",
        "Value": "key1value"
      },
      {
        "Key": "key2",
        "Value": "key2value"
      }
    ]
  )
)

dns_record = template.add_resource(
  RecordSetType("AuroraDnsRecord",
    Type = "CNAME",
    Name = hostname + "-" + args.environment + ".mythicmoon.com",
    HostedZoneName = "mythicmoon.com.",
    ResourceRecords = [ GetAtt("AuroraCluster", "Endpoint.Address") ],
    TTL = 300
  )
)

print(template.to_json())
