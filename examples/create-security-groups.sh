#!/bin/bash

profile=prd
vpc=vpc-be8145db

function ssh_only
{
    group_name="ssh_only"
    group_description="Security group for inbound ssh from internal addresses"

    echo "Group: $group_name"
    echo "Description: $group_description"

    result=`aws ec2 describe-security-groups --filters Name=group-name,Values=$group_name`
    count=`echo "$result" | jq -r '.SecurityGroups | length'`

    if [ $count -eq 1 ]; then
	echo "Already exists"
	return 0
    fi

    result=`aws ec2 create-security-group \
	--profile $profile \
	--group-name $group_name \
	--description $group_description \
	--vpc-id $vpc`

    if [ $? -ne 0 ]; then
	return 1
    fi

    sgid=`echo "$result" | jq -r .GroupId`

    result=`aws ec2 authorize-security-group-ingress \
	--profile $profile \
	--group-id $sgid \
	--protocol tcp \
	--port 22 \
	--cidr 146.197.0.0/16`

    if [ $? -ne 0 ]; then
	return 1
    fi

    result=`aws ec2 authorize-security-group-ingress \
	--profile $profile \
	--group-id $sgid \
	--protocol tcp \
	--port 22 \
	--cidr 10.0.0.0/8`

    if [ $? -ne 0 ]; then
	return 1
    fi
}

ssh_only
if [ $? -ne 0 ]; then
    exit 1
fi
echo ""
